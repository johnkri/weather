//
//  WeatherForecastBoundaries.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

protocol WeatherForecastViewContract: AnyObject {
    func displayCityName(_ cityName: String)
    func displayForecasts(_ viewModels: [WeatherForecastSectionViewModel])
}

protocol WeatherForecastPresenter: AnyObject {
    func start()
}
