//
//  WeatherForecastPresenter.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import WeatherCore

final class WeatherForecastPresenterImpl: WeatherForecastPresenter {

    private let cityName: String = "Paris"
    private let interactor: WeatherForecastInteractor
    private weak var view: WeatherForecastViewContract?

    init(view: WeatherForecastViewContract, interactor: WeatherForecastInteractor) {
        self.view = view
        self.interactor = interactor
    }

    // MARK: - WeatherForecastPresenter

    func start() {
        refresh()
    }

    // MARK: - Private

    private func refresh() {
        view?.displayCityName(cityName)
        interactor.getForecast(for: cityName) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let forecasts):
                self?.handleForecasts(forecasts)
            }
        }
    }

    private func handleForecasts(_ forecasts: [Forecast]) {
        let forecastsByDay = Dictionary(grouping: forecasts) {
            $0.date.midnight
        }.sorted(by: { $0.key < $1.key })

        view?.displayForecasts(
            forecastsByDay.map {
                WeatherForecastSectionViewModel(
                    title: DateFormatter.dayStyler.string(from: $0.key),
                    cells: $0.value.map(WeatherForecastCellViewModel.init)
                )
            }
        )
    }
}
