//
//  WeatherForecastSectionHeaderView.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit

public final class WeatherForecastSectionHeaderView: UITableViewHeaderFooterView {
    private lazy var headerLabel = createHeaderLabel()

    // MARK: - Lifecycle

    public override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setUp()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    // MARK: - Public

    public func configure(with title: String) {
        headerLabel.text = title
    }

    // MARK: - Private

    private func createHeaderLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white
        label.lineBreakMode = .byTruncatingTail
        label.accessibilityTraits = .header
        return label
    }

    private func setUp() {
        contentView.backgroundColor = .black
        contentView.addSubview(headerLabel)
        headerLabel.pinToSuperview(insets: UIEdgeInsets(top: 16.0, left: 16.0, bottom: 8.0, right: 16.0))
    }
}
