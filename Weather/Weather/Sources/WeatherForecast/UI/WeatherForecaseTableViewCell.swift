//
//  WeatherForecaseTableViewCell.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit

final class WeatherForecastTableViewCell: UITableViewCell {

    private lazy var iconImageView = UIImageView()
    private lazy var timeTitleLabel = UILabel()
    private lazy var descriptionLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }

    // MARK: - Public

    func configure(with viewModel: WeatherForecastCellViewModel) {
        iconImageView.load(url: viewModel.iconURL)
        timeTitleLabel.text = viewModel.timeTitle
        descriptionLabel.text = viewModel.description
    }

    // MARK: - Private

    private func setUp() {
        selectionStyle = .none
        let vStackView = UIStackView(verticalViews: [timeTitleLabel, descriptionLabel], spacing: 5.0)
        let hStackView = UIStackView(horizontalViews: [iconImageView, vStackView], spacing: 10.0)
        contentView.addSubview(hStackView)
        hStackView.alignment = .center
        hStackView.pinToSuperview(insets: UIEdgeInsets(value: 16.0))

        NSLayoutConstraint.activate([
            iconImageView.widthAnchor.constraint(equalToConstant: 24.0),
            iconImageView.heightAnchor.constraint(equalToConstant: 24.0)
        ])
    }
}
