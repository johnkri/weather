//
//  WeatherForecastViewController.swift
//  Weather
//
//  Created by John Kricorian on 88/03/2020.
//  Copyright © John. All rights reserved.
//

import UIKit

final class WeatherForecastViewController: UIViewController,
    WeatherForecastViewContract,
    UITableViewDataSource,
    UITableViewDelegate {

    private lazy var tableView = createTableView()

    private var viewModels: [WeatherForecastSectionViewModel] = []

    var presenter: WeatherForecastPresenter?

    // MARK: - Public

    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        presenter?.start()
    }

    // MARK: - WeatherForecastViewContact

    func displayCityName(_ cityName: String) {
        navigationItem.title = cityName
    }

    func displayForecasts(_ viewModels: [WeatherForecastSectionViewModel]) {
        self.viewModels = viewModels
        tableView.reloadData()
    }

    // MARK: - UITableViewDataSource

    public func numberOfSections(in tableView: UITableView) -> Int {
        return viewModels.count
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels[section].cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: WeatherForecastTableViewCell = tableView.dequeueCell(indexPath)
        cell.configure(with: viewModels[indexPath.section].cells[indexPath.row])
        return cell
    }

    // MARK: - UITableViewDelegate

    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: WeatherForecastSectionHeaderView = tableView.dequeueHeader()
        header.configure(with: viewModels[section].title)
        return header
    }

    // MARK: - Private

    private func setUp() {
        view.addSubview(tableView)
        tableView.pinToSuperview()
    }

    private func createTableView() -> UITableView {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(WeatherForecastTableViewCell.self)
        tableView.register(WeatherForecastSectionHeaderView.self)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return tableView
    }
}
