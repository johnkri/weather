//
//  WeatherForecastCellViewModel.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import WeatherCore

struct WeatherForecastCellViewModel {
    let iconURL: URL?
    let timeTitle: String
    let description: String
}

extension WeatherForecastCellViewModel {

    init(_ forecast: Forecast) {
        timeTitle = DateFormatter.timeStyler.string(from: forecast.date)
        if let weather = forecast.weathers.first {
            iconURL = URL(
                string: "http://openweathermap.org/img/w/\(weather.icon).png"
            )
            description = weather.description
        } else {
            iconURL = nil
            description = ""
        }
    }
}
