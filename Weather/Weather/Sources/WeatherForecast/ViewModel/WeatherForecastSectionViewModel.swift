//
//  WeatherForecastSectionViewModel.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

struct WeatherForecastSectionViewModel {
    let title: String
    let cells: [WeatherForecastCellViewModel]
}
