//
//  UIEdgeInsets+Utils.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit

public extension UIEdgeInsets {

    init(value: CGFloat) {
        self.init(top: value, left: value, bottom: value, right: value)
    }
}
