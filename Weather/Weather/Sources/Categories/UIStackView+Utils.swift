//
//  UIStackView+Utils.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit

public extension UIStackView {

    convenience init(verticalViews: [UIView], spacing: CGFloat = 0) {
        self.init(arrangedSubviews: verticalViews)
        self.axis = .vertical
        self.spacing = spacing
    }

    convenience init(horizontalViews: [UIView], spacing: CGFloat = 0) {
        self.init(arrangedSubviews: horizontalViews)
        self.axis = .horizontal
        self.spacing = spacing
    }
}
