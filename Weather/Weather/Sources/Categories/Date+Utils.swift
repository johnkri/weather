//
//  Date+Utils.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

extension Date {

    var midnight: Date {
        Calendar.current.date(
            bySettingHour: 0,
            minute: 0,
            second: 0,
            of: self
        ) ?? self
    }
}
