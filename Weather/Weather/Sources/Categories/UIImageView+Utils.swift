//
//  UIImage+Utils.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit

extension UIImageView {

    func load(url: URL?, placeholder: UIImage? = nil, session: URLSession = .shared, cache: URLCache = .shared) {
        guard let url = url else {
            self.image = placeholder
            return
        }
        let request = URLRequest(url: url)
        if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
            self.image = image
        } else {
            self.image = placeholder
            session.dataTask(with: request) { [weak self] (data, response, error) in
                guard
                    let data = data,
                    let response = response,
                    ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300,
                    let image = UIImage(data: data) else {
                        return
                }
                let cachedData = CachedURLResponse(response: response, data: data)
                cache.storeCachedResponse(cachedData, for: request)
                DispatchQueue.main.async {
                    self?.image = image
                }
            }.resume()
        }
    }
}
