//
//  Bundle+Settings.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

extension Bundle {

    var weatherAPIKey: String {
        infoDictionary?["WeatherAPIKey"] as? String ?? ""
    }

    var weatherAPIURL: URL {
        let urlString = infoDictionary?["WeatherAPIURL"] as? String ?? ""
        guard let url = URL(string: urlString) else {
            fatalError("Invalid Weather API URL")
        }
        return url
    }
}
