//
//  WeatherForecastRouter.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit
import WeatherCore

final class AppAssembly {

    static let shared = AppAssembly()

    private lazy var storageManager = createStorageManager()
    private lazy var weatherService = createWeatherService()

    // MARK: - Public

    func createWeatherForecastView() -> UIViewController {
        let view = WeatherForecastViewController()
        view.presenter = WeatherForecastPresenterImpl(
            view: view,
            interactor: createWeatherForecastInteractor()
        )
        return view
    }

    // MARK: - Private

    private func createWeatherForecastInteractor() -> WeatherForecastInteractor {
        WeatherForecastInteractorImpl(
            weatherService: weatherService,
            storageManager: storageManager
        )
    }

    private func createStorageManager() -> StorageManager {
        StorageManagerImpl()
    }

    private func createWeatherService() -> WeatherService {
        WeatherServiceImpl(
            weatherAPIKey: Bundle.main.weatherAPIKey,
            weatherAPIURL: Bundle.main.weatherAPIURL
        )
    }

    private weak var view: WeatherForecastViewContract?

    private var controller: UIViewController? {
        return view as? UIViewController
    }
}
