//
//  AppDelegate.swift
//  Weather
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        window.rootViewController = UINavigationController(
            rootViewController: AppAssembly.shared.createWeatherForecastView()
        )
        window.makeKeyAndVisible()
        return true
    }
}

