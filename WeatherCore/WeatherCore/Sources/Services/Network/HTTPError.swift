//
//  HTTPError.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public enum HTTPError: LocalizedError {
    case invalidURL
    case noStatusCode
    case noResponseData
    case invalidStatusCode(_ code: Int)
    case decodingFailure(_ error: Error)
    case other(_ error: Error)

    public init(_ error: Error) {
        if let error = error as? HTTPError {
            self = error
        } else if let error = error as? DecodingError {
            self = .decodingFailure(error)
        } else {
            self = .other(error)
        }
    }
}
