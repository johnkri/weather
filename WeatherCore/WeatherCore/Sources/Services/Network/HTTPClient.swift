//
//  HTTPClient.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public protocol HTTPClient {

    func performRequest<T: Decodable>(path: String,
                                      params: [String: String],
                                      completion: @escaping ((Result<T, HTTPError>) -> Void))
}
