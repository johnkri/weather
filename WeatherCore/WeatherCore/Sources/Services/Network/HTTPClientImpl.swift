//
//  HTTPClientImpl.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public final class HTTPClientImpl: HTTPClient {

    private let baseURL: URL
    private let urlSession = URLSession(configuration: .default)

    public init(baseURL: URL) {
        self.baseURL = baseURL
    }

    public func performRequest<T: Decodable>(path: String,
                                             params: [String: String],
                                             completion: @escaping ((Result<T, HTTPError>) -> Void)) {
        do {
            let request = try createRequest(path: path, params: params)
            urlSession.dataTask(with: request) { result in
                DispatchQueue.main.async {
                    completion(result
                        .flatMap(decodeResponseData)
                    )
                }
            }.resume()
        } catch {
            completion(.failure(HTTPError(error)))
        }
    }

    // MARK: - Private

    private func createRequest(path: String, params: [String: String]) throws -> URLRequest {
        var components = URLComponents(string: path)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }

        guard let url = components?.url(relativeTo: baseURL) else {
            throw HTTPError.invalidURL
        }
        return URLRequest(url: url)
    }
}

private func decodeResponseData<T: Decodable>(_ data: Data) -> Result<T, HTTPError> {
    do {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .secondsSince1970
        return .success(try decoder.decode(T.self, from: data))
    } catch {
        return .failure(.decodingFailure(error))
    }
}
