//
//  WeatherServiceResponse.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public struct WeatherServiceResponse: Decodable {
    public let city: City
    public let forecasts: [Forecast]

    enum CodingKeys: String, CodingKey {
        case city
        case forecasts = "list"
    }
}
