//
//  WeatherService.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public protocol WeatherService {

    func fetchDaily(for cityName: String, completion: @escaping ((Result<WeatherServiceResponse, HTTPError>) -> Void))
}
