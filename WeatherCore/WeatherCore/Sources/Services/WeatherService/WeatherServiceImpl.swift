//
//  WeatherServiceImpl.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public final class WeatherServiceImpl: WeatherService {

    private let weatherAPIKey: String

    private let client: HTTPClient

    public init(weatherAPIKey: String, weatherAPIURL: URL) {
        self.weatherAPIKey = weatherAPIKey
        self.client = HTTPClientImpl(baseURL: weatherAPIURL)
    }

    // MARK: - WeatherService

    public func fetchDaily(for cityName: String, completion: @escaping ((Result<WeatherServiceResponse, HTTPError>) -> Void)) {
        let params: [String: String] = [
            "q": cityName,
            "appid": weatherAPIKey
        ]
        client.performRequest(path: "data/2.5/forecast", params: params, completion: completion)
    }
}
