//
//  StorageManager.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public protocol StorageManager {
    func saveForcasts(_ forecasts: [Forecast])
    func retrieveForcasts(_ completion: @escaping (Result<[Forecast], Error>) -> Void)
}
