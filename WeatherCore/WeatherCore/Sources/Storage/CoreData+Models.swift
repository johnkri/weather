//
//  CoreData+Models.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

extension CDForecast {

    func configure(_ entity: Forecast) {
        self.date = entity.date

        let weathers: [CDWeather] = entity.weathers.map { weather in
            let object: CDWeather = CDWeather(entity: CDWeather.entity(), insertInto: managedObjectContext)
            object.configure(weather)
            return object
        }
        weathers.forEach { addToWeathers($0) }
    }

    func toEntity() -> Forecast? {
        guard let date = date, let weathers = weathers?.allObjects as? [CDWeather] else {
            return nil
        }
        return Forecast(
            date: date,
            weathers: weathers.compactMap { $0.toEntity() }
        )
    }
}

extension CDWeather {

    func configure(_ entity: Weather) {
        self.icon = entity.icon
        self.main = entity.main
        self.summary = entity.description
    }

    func toEntity() -> Weather? {
        guard let icon = icon, let main = main, let summary = summary else {
            return nil
        }
        return Weather(main: main, description: summary, icon: icon)
    }
}
