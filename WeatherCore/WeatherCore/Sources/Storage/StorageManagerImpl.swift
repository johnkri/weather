//
//  StorageManagerImpl.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import CoreData

public final class StorageManagerImpl: StorageManager {

    private let container: PersistentContainer

    private var mainContext: NSManagedObjectContext {
        return container.viewContext
    }

    public init() {
        container = PersistentContainer(name: "WeatherModel")
        container.loadPersistentStores { (_, error) in
            if let error = error {
                print(error)
            }
        }
    }

    // MARK: - StorageManager

    public func saveForcasts(_ forecasts: [Forecast]) {
        mainContext.perform {
            self.clearData()
            forecasts.forEach { forecast in
                let object = CDForecast(
                    entity: CDForecast.entity(),
                    insertInto: self.mainContext
                )
                object.configure(forecast)
            }
            self.saveContext()
        }
    }

    public func retrieveForcasts(_ completion: @escaping (Result<[Forecast], Error>) -> Void) {
        mainContext.perform {
            let fetchRequest: NSFetchRequest<CDForecast> = CDForecast.fetchRequest()
            fetchRequest.sortDescriptors = [
                NSSortDescriptor(key: "date", ascending: true)
            ]
            do {
                let forecasts = try self.mainContext.fetch(fetchRequest).compactMap { $0.toEntity() }
                completion(.success(forecasts))
            } catch {
                completion(.failure(error))
            }
        }
    }

    // MARK: - Private

    private func clearData() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = CDForecast.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
             try mainContext.execute(deleteRequest)
             try mainContext.save()
         } catch {
             print(error)
         }
    }

    private func saveContext() {
        if container.viewContext.hasChanges {
            do {
                try container.viewContext.save()
            } catch {
                print("An error occurred while saving: \(error)")
            }
        }
    }
}


// We have to subclass NSPersistentContainer to make sure it reads xcdatamodel in correct Bundle
private final class PersistentContainer: NSPersistentContainer { }
