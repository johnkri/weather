//
//  WeatherForecastInteractorImpl.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public final class WeatherForecastInteractorImpl: WeatherForecastInteractor {

    private let weatherService: WeatherService
    private let storageManager: StorageManager

    public init(weatherService: WeatherService, storageManager: StorageManager) {
        self.weatherService = weatherService
        self.storageManager = storageManager
    }

    public func getForecast(for cityName: String, completion: @escaping ((Result<[Forecast], HTTPError>) -> Void)) {
        storageManager.retrieveForcasts { result in
            if let cache = try? result.get() {
                completion(.success(cache))
            }
        }
        weatherService.fetchDaily(for: cityName) { [weak self] result in
            if let cache = try? result.get() {
                self?.storageManager.saveForcasts(cache.forecasts)
            }
            completion(result.map { $0.forecasts })
        }
    }
}
