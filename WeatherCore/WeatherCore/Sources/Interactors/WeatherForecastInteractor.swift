//
//  WeatherForecastInteractor.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public protocol WeatherForecastInteractor {

    func getForecast(for cityName: String, completion: @escaping ((Result<[Forecast], HTTPError>) -> Void))
}
