//
//  Weather.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public struct City: Decodable {
    public let id: Int
    public let name: String
    public let location: Location
    public let country: String

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case location = "coord"
        case country
    }
}
