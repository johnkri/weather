//
//  Weather.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public struct Weather: Decodable {
    public let main: String
    public let description: String
    public let icon: String
}
