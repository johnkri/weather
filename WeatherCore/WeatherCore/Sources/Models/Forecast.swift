//
//  Forecast.swift
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

public struct Forecast: Decodable {
    public let date: Date
    public let weathers: [Weather]

    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case weathers = "weather"
    }
}
