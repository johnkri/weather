//
//  File.swift
//  RATPCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

import Foundation

extension URLSession {

    func dataTask(with request: URLRequest,
                  completionHandler: @escaping (Result<Data, HTTPError>) -> Void) -> URLSessionDataTask {
        dataTask(with: request) { [weak self] (data, response, error) in
            let result = self?.transformResponse(data: data, response: response, error: error)
            result.map(completionHandler)
        }
    }

    // MARK: - Private

    private func transformResponse(data: Data?,
                                   response: URLResponse?,
                                   error: Error?) -> Result<Data, HTTPError> {

        guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
            return .failure(.noStatusCode)
        }
        if let data = data {
            if statusCode >= 200 && statusCode < 300 {
                return .success(data)
            } else {
                return .failure(.invalidStatusCode(statusCode))
            }
        } else {
            return .failure(.noResponseData)
        }
    }
}

