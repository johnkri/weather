//
//  WeatherCore.h
//  WeatherCore
//
//  Created by John Kricorian on 25/08/2022.
//  Copyright © John. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for WeatherCore.
FOUNDATION_EXPORT double WeatherCoreVersionNumber;

//! Project version string for WeatherCore.
FOUNDATION_EXPORT const unsigned char WeatherCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WeatherCore/PublicHeader.h>


